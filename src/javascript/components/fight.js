import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const firstFighterFullHealth = firstFighter.health,
      secondFighterFullHealth = secondFighter.health;
    const firstFighterHealthBar = document.getElementById('left-fighter-indicator'),
      secondFighterHealthBar = document.getElementById('right-fighter-indicator');
    const actions = {};
    let firstFighterCritcalAttackTimer = null,
      secondFighterCriticalAttackTimer = null,
      firstFighterCritcalAttackFlag = true,
      secondFighterCriticalAttackFlag = true;

    document.onkeydown = onkeyup = function (event) {
      actions[event.code] = event.type == 'keydown';
      let firstFighterAttack = false,
        secondFighterAttack = false,
        firstFighterBlock = false,
        secondFighterBlock = false,
        firstFighterCritcalAttack = false,
        secondFighterCriticalAttack = false;
      if (actions[controls.PlayerOneAttack] && !actions[controls.PlayerOneBlock]) {
        firstFighterAttack = true;
      }
      if (actions[controls.PlayerTwoAttack] && !actions[controls.PlayerTwoBlock]) {
        secondFighterAttack = true;
      }
      if (!actions[controls.PlayerOneAttack] && actions[controls.PlayerOneBlock]) {
        firstFighterBlock = true;
      }
      if (!actions[controls.PlayerTwoAttack] && actions[controls.PlayerTwoBlock]) {
        secondFighterBlock = true;
      }
      if (
        actions[controls.PlayerOneCriticalHitCombination[0]] &&
        actions[controls.PlayerOneCriticalHitCombination[1]] &&
        actions[controls.PlayerOneCriticalHitCombination[2]] &&
        firstFighterCritcalAttackFlag
      ) {
        firstFighterCritcalAttack = true;
        firstFighterCritcalAttackFlag = false;
        firstFighterCritcalAttackTimer = setTimeout(() => {
          firstFighterCritcalAttackFlag = true;
        }, 10000);
      }
      if (
        actions[controls.PlayerTwoCriticalHitCombination[0]] &&
        actions[controls.PlayerTwoCriticalHitCombination[1]] &&
        actions[controls.PlayerTwoCriticalHitCombination[2]] &&
        secondFighterCriticalAttackFlag
      ) {
        secondFighterCriticalAttack = true;
        secondFighterCriticalAttackFlag = false;
        secondFighterCriticalAttackTimer = setTimeout(() => {
          secondFighterCriticalAttackFlag = true;
        }, 10000);
      }
      switch (true) {
        case firstFighterAttack && !secondFighterBlock: {
          secondFighter.health -= getDamage(firstFighter, secondFighter);
          const healthBarWidth = (secondFighter.health / secondFighterFullHealth) * 100;
          secondFighterHealthBar.style.width = `${healthBarWidth > 0 ? healthBarWidth : 0}%`;
          break;
        }
        case secondFighterAttack && !firstFighterBlock: {
          console.log(getDamage(secondFighter, firstFighter));
          firstFighter.health -= getDamage(secondFighter, firstFighter);
          const healthBarWidth = (firstFighter.health / firstFighterFullHealth) * 100;
          firstFighterHealthBar.style.width = `${healthBarWidth > 0 ? healthBarWidth : 0}%`;
          break;
        }
        case firstFighterCritcalAttack: {
          secondFighter.health -= firstFighter.attack * 2;
          const healthBarWidth = (secondFighter.health / secondFighterFullHealth) * 100;
          secondFighterHealthBar.style.width = `${healthBarWidth > 0 ? healthBarWidth : 0}%`;
          break;
        }
        case secondFighterCriticalAttack: {
          firstFighter.health -= secondFighter.attack * 2;
          const healthBarWidth = (firstFighter.health / firstFighterFullHealth) * 100;
          firstFighterHealthBar.style.width = `${healthBarWidth > 0 ? healthBarWidth : 0}%`;
          break;
        }
        default:
          let winner = null;
          if (secondFighter.health <= 0) {
            winner = firstFighter;
          } else if (firstFighter.health <= 0) {
            winner = secondFighter;
          }
          if (winner) {
            resolve(winner);
          }
          break;
      }
    };
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage <= 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  // return hit power
  const randomNumber = Math.random() * (2 - 1 + 1) + 1;
  return randomNumber * fighter.attack;
}

export function getBlockPower(fighter) {
  // return block power
  const randomNumber = Math.random() * (2 - 1 + 1) + 1;
  return randomNumber * fighter.defense;
}
