import { showModal } from './modal';
import App from '../../app';

export function showWinnerModal(fighter) {
  // call showModal function
  const rootEl = document.getElementById('root');
  showModal({
    title: `Victory!`,
    bodyElement: `${fighter.name}`,
    onClose: () => {
      rootEl.innerHTML = '';
      new App();
    },
  });
}
