import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const fighterHtml = fighter
    ? `<img src="${fighter.source}" height="200px" width="auto" alt="${fighter.name}">
	<ul class="fighter__char">
	<li><span>Name:</span> ${fighter.name}</li>
	<li><span>Health:</span> ${fighter.health}</li>
	<li><span>Attack:</span> ${fighter.attack}</li>
	<li><span>Defense:</span> ${fighter.defense}</li>
	</ul>`
    : '';

  fighterElement.innerHTML = fighterHtml;
  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
